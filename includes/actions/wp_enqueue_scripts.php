<?php


add_action('wp_enqueue_scripts', 'msco_enqueue_scripts');
function msco_enqueue_scripts() {
  // CDNs
  wp_enqueue_style('font-awesome', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css');

  // Template enqueues
  wp_enqueue_style('default-template', get_template_directory_uri() . '/assets/css/default.css');
}
