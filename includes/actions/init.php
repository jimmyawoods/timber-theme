<?php

add_action('init', 'msco_init');
function msco_init() {
  msco_register_nav_menus();
}


function msco_register_nav_menus() {
  register_nav_menus(array(
    'header-menu' => __('Header Menu'),
    'footer-menu' => __('Footer Menu'),
  ));
}
